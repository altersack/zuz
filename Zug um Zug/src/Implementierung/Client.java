package Implementierung;

public abstract class Client {
	
	private ClientModel model;
	private EventHandler evehandler;
	private int id;
	
	public Client(ClientModel model, EventHandler evehandler, int id) {
		this.model = model;
		this.evehandler = evehandler;
		this.id = id;
	}
	
	
	/**
	 * 
	 * @return true if move is expected, false if move from other player will follow
	 */
	public boolean binIchDran(){
		return model.getIdOfCurrentPlayer()==this.id;
	}
	
	public abstract void performMove();
	
	
	/**
	 * 
	 * @param args
	 */
	public void main(String[] args){
		//TODO implement me
	}

	
	public void handleEvent(Event e){
		this.evehandler.handleEvent(e);
	}
	
}
