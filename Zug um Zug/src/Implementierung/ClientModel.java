package Implementierung;

import java.util.*;

public class ClientModel extends Model{
	

	private ArrayList<SpielerdatenSimple> playerList;
	private Spielerdaten meineDaten;
	private int[] initialeKarten;

	/** 
	 * Konstruktor mit Parametern Spielfeld und der maximalen Streckenl�nge
	 * @param map
	 * @param maxtracks
	 */
	public ClientModel(Spielfeld map, int maxtracks) {
		super(map, maxtracks);
		this.playerList = new ArrayList<SpielerdatenSimple>();
	}


	
	/**
	 * f�gt einen neuen Spieler in die Spielerliste ein
	 * @param sp SpielerdatenSimple
	 */
	public void addPlayer(SpielerdatenSimple sp) {
		this.playerList.add(sp);
	}
	
	/**
	 * merkt sich die eigenen Daten und f�gt sie der Spielerliste hinzu
	 * @param s Spielerdaten vom sich selbst
	 */
	public void addMyData(Spielerdaten s){
		this.meineDaten = s;
		this.playerList.add(s);
	}

	/**
	 * L�scht einen Spieler aus der Spielerliste
	 * @param id des Spielers
	 */
	public void removePlayer(int id) {
		// TODO Auto-generated method stub
	}

	/**
	 * f�gt eine neue Handkarte einem Spieler hinzu
	 * @param colour
	 * @param id des Spielers
	 */
	public void addHandCards(String colour, int id) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * setzt den Besitzer einer Strecke mit ID idt auf den Spieler mit ID idp
	 * @param idt
	 * @param idp
	 */
	public void setOwner(int idt, int idp) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * f�gt einem Spieler neue Missionskarten hinzu
	 * @param m: Liste aller Missionskarten
	 * @param id des Spielers
	 */
	public void addMissionCard(String[] m, int id) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * �berpr�ft, ob ein Spieler schon die H�chstgrenze der Streckenl�nge �berschritten hat
	 * @return true, wenn das Spiel vorbei ist
	 */
	public boolean isOver() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * L�scht eine Handkarte der Farbe colour 
	 * @param colour
	 */
	public void removeHandCard(String colour){
		
	}

	public ArrayList<SpielerdatenSimple> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(ArrayList<SpielerdatenSimple> playerList) {
		this.playerList = playerList;
	}

	public Spielerdaten getMeineDaten() {
		return meineDaten;
	}

	public void setMeineDaten(Spielerdaten meineDaten) {
		this.meineDaten = meineDaten;
	}

	public int[] getInitialeKarten() {
		return initialeKarten;
	}

	public void setInitialeKarten(int[] initialeKarten) {
		this.initialeKarten = initialeKarten;
	}

	@Override
	public void setNextPlayer() {
		this.weristdran = (this.weristdran+1)%this.playerList.size();
	}

	@Override
	public int getIdOfCurrentPlayer() {
		return this.playerList.get(weristdran).id;
	}


}
