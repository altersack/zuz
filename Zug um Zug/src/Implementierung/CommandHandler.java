package Implementierung;

public class CommandHandler {

	private Server server;
	private MarsenneTwister randomGen;
	private Model m;
	private boolean takeSecondCardFlag;
	private int missionflag;
	
	/**
	 * 
	 * @param server
	 * @param randomGen
	 * @param m
	 */
	public CommandHandler(Server server,MarsenneTwister randomGen,Model m){
		this.m = m;
		this.randomGen = randomGen;
		this.server = server;
	}
	
	/**
	 * 
	 * @param c: Command
	 */
	
	public void handleCommand(Command c){
		
	}
	/**
	 * 
	 * @param name
	 * @param id: des Spielers
	 */
	
	public void register(String name, int id){
		
	}
	
	
	/**
	 * 
	 * @param id: Spieler der das Spiel verl�sst
	 */
	public void leave(int id){
		
	}
	
	
	/**
	 * 
	 * @param id
	 */
	public void getMission(int id){
		
	}
	/**
	 * 
	 * @param m : Mission Array to return
	 * @param id : id des Spielers
	 */
	
	public void returnMissions(Mission[] m,int id){
		
	}
	
	/**
	 * 
	 * @param idStrecke
	 * @param k : Farbe der Strecke
	 * @param locs
	 * @param idSpieler
	 */
	
	public void buildTrack(int idStrecke,kind k,int locs,int idSpieler){
		
	}
	
	/**
	 * 
	 * @param id : SpielerId
	 */
	
	public void takeCard(int id){
		
	}
	
	/**
	 * 
	 * @param id : SpielerID
	 */
	public void takeSecretCard(int id){
		
	}
	
	/**
	 * 
	 * @param id : New Observer id
	 */
	
	public void newObserver(int id){
		
	}
	/**
	 * 
	 * @param id:SpielerID
	 */
	public void timeOut(int id){
		
	}
	
	/**
	 * 
	 * @param locs
	 * @param id : des Spielers
	 */
	
	public void payTunnel(int locs,int id){
		
	}
	
	
	
}
