package Implementierung;

public class EventHandler {
	
	int id;
	ClientModel model;
	
	/**
	 * 
	 * @param id
	 * @param model
	 */
	public EventHandler(int id, ClientModel model) {
		this.id = id;
		this.model = model;
	}
	
	/**
	 * 
	 * @param e
	 */
	public void handleEvent(Event e){
		//TODO
	}
	
	private void gameState(int numPlayers){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 */
	private void sendId(int id){
		//TODO
	}
	
	/**
	 * 
	 * @param k
	 * @param count
	 */
	private void newCards(kind k, int count){
		//TODO
	}
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @param points
	 */
	private void mission(int start, int end, int points){
		//TODO
	}
	
	/**
	 * 
	 * @param missions
	 */
	private void newMissions(Mission[] missions){
		//TODO
	}
	
	/**
	 * 
	 * @param msg
	 */
	private void failure(String msg){
		//TODO
	}
	
	/**
	 * 
	 * @param msg
	 */
	private void die(String msg){
		//TODO
	}
	
	/**
	 * 
	 * @param points
	 * @param id
	 */
	private void finalPoints(int points, int id){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 * @param done
	 * @param failed
	 */
	private void finalMissions(int id, Mission[] done, Mission[] failed){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 * @param length
	 */
	private void longestTrack(int id, int length){
		//TODO
	}
	
	/**
	 * 
	 */
	private void gameStart(){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 */
	private void playerLeft(int id){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 * @param name
	 */
	private void newPlayer(int id, String name){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 * @param amount
	 */
	private void tookMissions(int id, int amount){
		//TODO
	}
	
	/**
	 * 
	 * @param idp
	 * @param idt
	 * @param k
	 * @param locs
	 */
	private void trackBuilt(int idp, int idt, kind k, int locs){
		//TODO
	}
	
	/**
	 * 
	 * @param k
	 */
	private void newOpenCard(kind k){
		//TODO
	}
	
	/**
	 * 
	 * @param k
	 * @param id
	 */
	private void tookCard(kind k, int id){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 */
	private void tookSecretCard(int id){
		//TODO
	}
	
	/**
	 * 
	 * @param length
	 */
	private void maxTrackLength(int length){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 * @param start
	 * @param end
	 * @param k
	 * @param tunnel
	 */
	private void newTrack(int id, int start, int end, kind k, boolean tunnel){
		//TODO
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param id
	 * @param name
	 */
	private void newCity(int x, int y, int id, String name){
		//TODO
	}
	
	/**
	 * 
	 * @param id
	 */
	private void winner(int id){
		//TODO
	}
	
	/**
	 * 
	 * @param idp
	 * @param idt
	 * @param addCost
	 * @param color
	 * @param locs
	 */
	private void tunnelBuilt(int idp, int idt, int addCost, kind color, int locs){
		//TODO
	}
	
	/**
	 * 
	 * @param idp
	 * @param idt
	 */
	private void tunnelNotBuilt(int idp, int idt){
		//TODO
	}
	
	/**
	 * 
	 * @param costs
	 */
	private void additiocalCost(int costs){
		//TODO
	}
}
