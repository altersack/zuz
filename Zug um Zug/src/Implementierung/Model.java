package Implementierung;

import java.util.*;

public abstract class Model {
	
	private Spielfeld map;
	protected int weristdran;
	private ArrayList<Integer> fieldCards;
	private final int maxtracks;
	
	
	public Model(Spielfeld map, int maxtracks){
		this.map=map;
		this.maxtracks=maxtracks;
	}
	

	public abstract void addPlayer(SpielerdatenSimple sp);
	
	public Spielfeld getSpielfeld() {
		return map;
	}


	public int getMaxtracks() {
		return maxtracks;
	}


	public abstract void removePlayer(int id);
	/**
	 * F�gt eine Stadt s zur Karte hinzu
	 * @param s
	 */
	public void addCity(String name, int id, int x, int y){
		map.addCity(new Stadt( name,  id,  x,  y));
	}
	/**
	 * F�gt eine Handkarte der Farbe colour zum Spieler mit der ID id hinzu
	 * @param colour
	 * @param id
	 */
	public abstract void addHandCards(String colour, int id); //colour ist vom Typ kind
	
	/**
	 * F�gt eine neue Strecke s zur Karte hinzu
	 * @param s
	 */
	public void addTrack(Stadt start,Stadt ziel,int id, int length,String color, boolean tunnel){
		map.addTrack(new Strecke(start,ziel,id,length,color,tunnel));
	}
	/**
	 * Deckt eine neue offene Karte auf, kann nur angewandt werden, 
	 * wenn die vorher gezogene Karte bereits gel�scht wurde, also das
	 * entsprechende Feld null ist.
	 * @param card
	 */
	public void setzeOffeneKarten(String card){
		
	}
	
	/**
	 * L�scht die Karte mit der Farbe colour aus den offenen Karten
	 * @param colour
	 */
	public void loescheOffeneKarte(String colour){
		
	}
	
	/**
	 * Liefert die Liste der offenen Karten zur�xk, in der Liste sind die Farben als ints gespeichert
	 * @return
	 */
	public ArrayList<Integer> getOpenCards(){
		return this.fieldCards;
	}
	/**
	 * �berpr�ft, ob die Mission vom Spieler mit der ID id bereits erf�llt ist.
	 * @param id
	 * @param mission
	 * @return
	 */
	public boolean missionComplete(int id, String mission){
		return false;
	}
	
	public abstract void setOwner(int idt, int idp);
	
	/** 
	 * setzt den weristdran auf den aktuellen Spieler
	 */
	public abstract void setNextPlayer();
	
	
	/**
	 * 
	 * @return id des Spielers der an der Reihe ist
	 */
	public abstract int getIdOfCurrentPlayer();
	
	public abstract void addMissionCard(String[] m, int id);
	
	public abstract boolean isOver();
	
	
}
