package Implementierung;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Parser {

	/**
	 * @author Jeeeesuuuuus
	 * @param fileName
	 * @return complete ServerModel
	 * @throws IOException
	 */
	public static ServerModel parse(String fileName) throws IOException {

		ServerModel q = new ServerModel(new Spielfeld(),-1,null,null);
		int maxtracks = 0;
		ArrayList<String> cardstack= new ArrayList<String>();
		ArrayList<String> missionstack= new ArrayList<String>();
		BufferedReader s;
		Spielfeld map;
		String zeile;
		String[] input;


		try{
		File f = new File(fileName);
		 map = new Spielfeld();
		s = new BufferedReader(new FileReader(f));
		//System.out.println(s);
		}catch (Exception ex){
			throw new IOException("FileNotFoundException");
		}


		zeile = s.readLine();
				/*
				 * liest Initiale Karten ein und speichert jede Karte nacheinander mit seiner Anzahl in einer
				 * Arraylist
				 */
				for(int i=0; i<9;i++){
					 zeile=s.readLine();
					 /*System.out.println(zeile);*/
					 input = zeile.split(",");
					 for(int a = 0;a <Integer.parseInt(input[1]);a++){

						 cardstack.add(input[0]);
					 }
				 }
				/*speichert Maxtracks*/
					 zeile = s.readLine();
					 input = zeile.split(",");
					 maxtracks = Integer.parseInt(input[1]);
					/*System.out.println(zeile);*/

			/*
			 * Ab hier beginnt das Parsen der Staedte:
			 * s.readLine()�berspringt das Wort Staedte
			 * maap.addCity(n) f�gt die Stadt dem Spielfeld hinzu
			 */
					s.readLine();
					int counter = 0;
				 while((zeile = s.readLine()).equals("Strecken:")==false){
					 input= zeile.split(",");
					/* System.out.println("Test");
					 System.out.println(input[0]);*/
//					 Stadt n = new Stadt(input[0],counter,Integer.parseInt(input[1]),Integer.parseInt(input[2]));
//					 map.addCity(n);
					 q.addCity(input[0],counter,Integer.parseInt(input[1]),Integer.parseInt(input[2]));
					/* System.out.println(n);*/
					 counter++;
				 }

			/*Hier werden die Strecken geparst
			 * boolean tunnel ueberprueft ,ob die Strecke ein Tunnel ist oder nicht
			 * input[0]/[1]: id der Staedte, die schon vorher geparst worden sind(erste start,zweite ziel)
			 * input[2]:Laenge der Strecke
			 * input[3]:Farbe der Strecke
			 * input[4]:Tunnel"T" oder Normal "N"
			 */
				 counter = 0;
				 while((zeile = s.readLine()).equals("Auftraege:")==false){

					 input = zeile.split(",");
					 Stadt start = map.cities.get(Integer.parseInt(input[0]));
					 Stadt ziel = map.cities.get(Integer.parseInt(input[1]));

					 boolean tunnel = input[4].equals("T");

					 //Strecke str = new Strecke(start,ziel,counter,Integer.parseInt(input[2]),input[3],tunnel);
					 q.addTrack(start, ziel, counter, Integer.parseInt(input[2]), input[3], tunnel);
					counter++;
									 }


				 /*
				  * Todo....implement Mission parsing
				  */

				 while((zeile = s.readLine())!= null){
					 input= zeile.split(",");



				 }




				 ServerModel w = new ServerModel(map,maxtracks,cardstack,missionstack);

		s.close();
		return q;



	}

}
