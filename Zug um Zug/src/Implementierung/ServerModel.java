package Implementierung;

import java.util.ArrayList;

public class ServerModel extends Model{
	
	
	 ArrayList<Integer> cardstack =new ArrayList<Integer>();
	 ArrayList<Mission> missionstack =new ArrayList<Mission>();
	 ArrayList<Spielerdaten> playerList =new ArrayList<Spielerdaten>();
	 
	 
	 /**
	  * 
	  * @param cs cardstack
	  * @param ms missionstack
	  */
	 public ServerModel(Spielfeld map, int maxtracks, ArrayList<Integer> cs, ArrayList<Mission> ms){
		 super(map, maxtracks);
		 this.missionstack=ms;
		 this.cardstack=cs;
	 }
	
	/**
	 * Spieler sp wird zur Spielerliste hinzugef�gt
	 * @param sp
	 */
	 public void addPlayer(SpielerdatenSimple sp){
		 
	 }

	 
	 public ArrayList<Integer> getCards(){
		 return cardstack;
	 }
	 public ArrayList<Mission> getMissions(){
		 return missionstack;
	 }

	 public ArrayList<Spielerdaten> getPlayerList() {
		return playerList;
	}

	 
	/**
	  * Der Spieler mit der ID id wird aus der Spielerliste gel�scht
	  * @param id
	  */
	public void removePlayer(int id) {
		// TODO Auto-generated method stub
		
	}


	/**
	 * f�gt eine neue Handkarte mit der Farbe colour zum Spieler mit der ID id hinzu
	 * @param colour
	 * @param id
	 */
	public void addHandCards(String colour, int id) {
		// TODO Auto-generated method stub
		
	}


	/**
	 * Legt den Besitzer der Strecke idt auf den Spieler mit der ID idp fest.
	 * @param idt
	 * @param idp
	 */
	public void setOwner(int idt, int idp) {
		
		
	}


	/**
	 * F�gt dem Spieler mit ID id eine Liste von Missionen m hinzu
	 * @param m
	 * @param id
	 */
	public void addMissionCard(Mission[] m, int id) {
		// TODO Auto-generated method stub
		
	}


	/**
	 * Stellt fest, ob einer der Spieler die H�chstgrenze der Streckenl�nge �berschritten hat
	 * @return true, wenn das Spiel vorbei ist
	 */
	public boolean isOver() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Ermittelt, welcher Spieler die l�ngste Strecke gebaut hat
	 * @return Id des Spielers mit der l�ngsten Route
	 */
	public int longestRoute(){
		return 0;
	}
	
	/**
	 * Erzeugt eine zuf�llig gezogene Ressourcenkarte und gibt die Farbe zur�ck
	 * @param m
	 * @return colour
	 */
	public String getRandomCard(MarsenneTwister m){
		return "";
	}
	/**
	 * L�scht eine Handkarte mit Farbe colour des Spielers mit der entsprechenden Id
	 * @param id
	 * @param colour
	 */
	public void removeHandCard(int id, kind colour){
		
	}
	/**
	 * erzeugt zuf�llig eine Liste mit drei neuen Auftr�gen
	 * @param m
	 * @return Liste mit drei Auftr�gen
	 */
	public String[] getNewMissions(MarsenneTwister m){
		return null;
	}

	
	/**
	 * erh�ht den "wer ist dran" index
	 */
	@Override
	public void setNextPlayer() {
		this.weristdran = (this.weristdran+1)%this.playerList.size();
	}


	@Override
	public int getIdOfCurrentPlayer() {
		return this.playerList.get(this.weristdran).id;
	}
	
	
	
}
