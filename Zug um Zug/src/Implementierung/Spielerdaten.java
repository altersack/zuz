package Implementierung;

import java.util.ArrayList;

public class Spielerdaten extends SpielerdatenSimple{

	int[] handcards;
	ArrayList<Mission> missions = new ArrayList<Mission>();
	int fails;
	
	public Spielerdaten(int id, String name) {
		super(id, name);
		this.fails = 0;
		this.handcards = new int[9];
	}
	
	
	public void addCard(kind color, int num){
		//TODO implement me
	}
	
	
	public void addMission(Mission m){
		this.missions.add(m);
	}
	
	public void removeMission(Mission m){
		//TODO implement me
	}
	
	public int[] getHandCards(){
		return this.handcards;
	}
	
	public int getFails(){
		return this.fails;
	}
	
	/**
	 * 
	 * @param x number of added fails
	 */
	public void setFails(int x){
		this.fails += x;
	}

}
