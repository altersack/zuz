package Implementierung;

import java.util.ArrayList;

public class SpielerdatenSimple {
	
	int id;
	int points;
	String name;
	int mcount;
	int ccount;
	int uWagons;
	ArrayList<Strecke> tracks;
	
	
	
	public SpielerdatenSimple(int id, String name) {
		this.id = id;
		this.points = 0;
		this.name = name;
		this.ccount = 4;
		this.mcount = 0;
		this.uWagons = 0;
		this.tracks = new ArrayList<Strecke>();
	}
	
	/**
	 * 
	 * @param x number of points to add
	 */
	public void addPoints(int x){
		this.points += x;
	}
	
	public int getPoints(){
		return this.points;
	}
	
	public int getUsedWaggons(){
		return this.uWagons;
	}
	
	public int getNumMissions(){
		return this.mcount;
	}
	
	/**
	 * 
	 * @param x number of added misssions
	 */
	public void tookMissions(int x){
		this.mcount += x;
	}
	
	/**
	 * adds track, lowers usedWaggons, lowers cardcount 
	 * 
	 * @param s track
	 */
	public void addTrack(Strecke s){
		this.tracks.add(s);
		int l = s.getLength();
		this.uWagons += l;
		this.ccount -= l;
		if (l < 3){
			this.addPoints(l);
		}
		else if (l==3){
			this.addPoints(4);
		}
		else if (l==4){
			this.addPoints(7);
		}
		else if (l==5){
			this.addPoints(10);
		}
		else{
			this.addPoints(15);
		}
	}
	
	/**
	 * 
	 * @return number of cards the player has left
	 */
	public int getNumCards(){
		return this.ccount;
	}
	
	/**
	 * 
	 * @param x number of added cards
	 */
	public void tookCard(){
		this.ccount++;
	}

}
