package Implementierung;

import java.util.ArrayList;

public class Spielfeld {
	ArrayList<Stadt> cities = new ArrayList<Stadt>();
	ArrayList<Strecke> tracks = new ArrayList<Strecke>();

	public Spielfeld() {
	}

	/**
	 *
	 * @param a
	 *            Besitzer id
	 * @param b
	 *            id der zu bauenden Strecke
	 */
	public void setzeBesitzer(SpielerdatenSimple a, int b) {
		tracks.get(b).setOwner(a);
	}

	public void addCity(Stadt s) {
		cities.add(s);
	}

	public ArrayList<Stadt> getCity(){
		return cities;
	}

	public ArrayList<Strecke> getTrack(){
		return tracks;
	}

	public void addTrack(Strecke s){
		tracks.add(s);
	}


}
