package Implementierung;

import java.util.ArrayList;

public class Stadt {

	private String name;
	int id;
	private int x;
	private int y;
	private ArrayList<Strecke> list = new ArrayList<Strecke>();

	/**
	 *
	 * @param name
	 * @param id
	 * @param x
	 *            Koordinate
	 * @param y
	 *            Koordinate
	 */
	public Stadt(String name, int id, int x, int y) {
		this.name = name;
		this.id = id;
		this.x = x;
		this.y = y;

	}

	public String toString() {
		return "" + name + "," + id + "," + x + "," + y;
	}

	public void kanteHinzufuegen(Strecke s) {
		list.add(s);
	}

	public int getId(){
		return id;
	}

	public int getX(){
		return x;

	}
	public int getY(){
		return y;
	}

	public ArrayList<Strecke> getList(){
		return list;
	}

	public String getName(){
		return name;

	}

}
