package Implementierung;

public class Strecke {
	private Stadt start;
	private Stadt ziel;
	private int id;
	private int length;
	private String color;
	private SpielerdatenSimple owner;
	private boolean tunnel;


	/**
	 * Gibt den Startpunkt der Strecke zur�ck
	 * @return start
	 */
	public Stadt getStart() {
		return start;
	}
	/**
	 * Gibt den Endpunkt der Strecke zur�ck
	 * @return end
	 */
	public Stadt getZiel() {
		return ziel;
	}
	/**
	 * gibt die Streckenid zur�ck
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * gibt die Farbe der Strecke zur�ck
	 * @return colour
	 */
	public String getColor() {
		return color;
	}
	/**
	 * gibt ein boolean zur�ck, ob die Strecke ein Tunnel ist oder nicht
	 * @return tunnel
	 */
	public boolean isTunnel() {
		return tunnel;
	}

	public Strecke(Stadt start,Stadt ziel,int id, int length,String color, boolean tunnel) {
		this.start = start;
		this.ziel = ziel;
		this.id = id;
		this.length = length;
		this.color = color;
		this.owner = null;
		this.tunnel = tunnel;
		start.kanteHinzufuegen(this);
		ziel.kanteHinzufuegen(this);
	}

	/**
	 *
	 * @return id of owner, -1 if no owner
	 */
	public int getOwner(){
		if (this.owner==null){
			return -1;
		}
		return owner.id;
	}

	/**
	 * @author Your Mom
	 * @param owner
	 * @return length
	 */
	public int setOwner(SpielerdatenSimple owner){
		this.owner = owner;
		return length;
	}

	public int getLength(){
		return length;
	}

	 @Override
	public String toString() {
		return ""+start.id + ","+ziel.id+","+length+","+owner+tunnel;
	}
}
