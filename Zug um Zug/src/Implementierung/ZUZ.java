package Implementierung;

import java.io.IOException;

public class ZUZ {
	
	int numPlayer;
	int seed;
	CommandHandler handler;
	ServerModel model;
	Server server = new Server();
	MarsenneTwister twister;
	

	
	 public static void main (String[] args){
		 
		 try {
			 String a = "C:/Users/Sven/workspace/Zug um Zug/src/Implementierung/Map";
			Parser.parse(a);
			//parse.createGraphFromMap("C:/Users/Sven/Desktop/zuz/Zug um Zug/src/Tests/MapTest");
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 
	 /**
	 * @param String[] args 1. port 2. mapfile 3. seed 4. num players
	 */
	 public void startGame(String[] args){
		 try{
		 this.model = Parser.parse(args[1]);}
		 catch(IOException e){
			 e.printStackTrace();
		 }
		 if (args[2]!= null)
			 this.seed = Integer.parseInt(args[2]);
		 this.numPlayer = Integer.parseInt(args[3]);
		 while(model.getPlayerList().size()<this.numPlayer){
			 Command c = this.server.pollNextCommand();
			 this.handler.handleCommand(c);
		 } 
	 }
	 
	 /**
	  * 
	  * @param Command c
	  */
	 public void handleCommand(Command c){
		 handler.handleCommand(c);
	 }
	 
	 public  static void endGame(){
		 //TODO implement me
	 }
	 
}
