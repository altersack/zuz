package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Implementierung.*;

public class ClientTests {
	
	ClientModel model;
	EventHandler eve;
	
	@Before
	public void init(){
		model = new ClientModel(new Spielfeld(), 3);
		Spielerdaten hans = new Spielerdaten(0, "hans");
		Spielerdaten karl = new Spielerdaten(2, "karl");
		model.addPlayer(hans);
		model.addPlayer(karl);
	}

	@Test
	public void binIchDranTest() {
		GuiSpieler karl = new GuiSpieler(model, eve, 2);
		GuiSpieler hans = new GuiSpieler(model, eve, 0);
		assertTrue("Karl ist aber nicht an der Reihe", !karl.binIchDran());
		assertTrue("Hans ist aber an der Reihe", hans.binIchDran());
		model.setNextPlayer();
		assertTrue("Karl ist aber an der Reihe", karl.binIchDran());
		assertTrue("Hans ist aber nicht an der Reihe", !hans.binIchDran());
		model.setNextPlayer();
		assertTrue("Karl ist aber nicht an der Reihe", !karl.binIchDran());
		assertTrue("Hans ist aber an der Reihe", hans.binIchDran());
	}
}
