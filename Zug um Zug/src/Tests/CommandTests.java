package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Implementierung.*;

public class CommandTests {

	CommandHandler ch;
	ServerModel model;
	
	@Before
	public void init(){
		Server server = new Server();
		Random r = new MarsenneTwister();
		model = new testSetUp().serverSetUp1();
		                  
		ch = new CommandHandler(server, r, model, false, false);
	}

	@Test
	public void registerTest(){
		ch.register("Tester", 5);
		Spielerdaten spieler = model.getPlayer(5);
		assertTrue(spieler.getName().equals("Testet"));
	}
	
	@Test
	public void buildTest(){
		Spielerdaten sp = new Spielerdaten(8, "SP");
		Strecke s = new Strecke(new Stadt("1",6,1,1),new Stadt("1",6,1,1),5,4,"RED",false);
		sp.addCard(RED, 2);
		sp.addCard(ALL, 1);
		ch.buildTrack(5, RED, 2, 8);
		assertTrue(s.getOwner() == -1);
		assertTrue(sp.getHandCards().length == 3);
		sp.addCard(ALL, 1);
		ch.buildTrack(5, RED, 2, 8);
		assertTrue(s.getOwner() == 8);
		assertTrue(sp.getHandCards().length == 0);
	}
	
	@Test
	public void takeCardTest(){
		model.addPlayer(new Spielerdaten(8, "SP"));
		Spielerdaten spieler = model.getPlayer(8);
		assertTrue(spieler.getId() == 4); 
		assertTrue(spieler.getHandCards().length == 0);
		ch.takeCard(RED, 4);
		assertTrue(spieler.getHandCards().length == 1);
	}
	
	
}
