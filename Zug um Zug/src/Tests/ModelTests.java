package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Implementierung.*;

public class ModelTests {

	ClientModel testmodel;
	
	/* Model:
	 * 
	 * ClientModel:
	 *  - addPlayer:
	 *  - removePlayer
	 *  - addHandCards
	 *  - setOwner
	 *  - addMissionCard
	 *  - isOver
	 */
	
	@Before
	public void setUp() {
	
	ClientModel testmodel = TestSetUp.clientSetUp1();
	}
	
	@Test
	public void addPlayerTest() {
		
		ClientModel clientModel;
		Spielerdaten spielerdaten;
		
		spielerdaten = new Spielerdaten(2, "BVB");
		clientModel = testmodel;
	
		ArrayList<SpielerdatenSimple> playerList = testmodel.getPlayerList();
		
		
		assert(!playerList.contains(spielerdaten));
		clientModel.addPlayer(spielerdaten);
		assert(playerList.contains(spielerdaten));
		
	}
	
	@Test
	public void removePlayer() {
		
		ClientModel clientModel;
		int id;
		
	}

}
