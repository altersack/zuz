package Tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import Implementierung.Parser;
import Implementierung.ServerModel;
import Implementierung.Spielfeld;
import Implementierung.ZUZ;

public class ParserTests {
	@Test
	public void test973486() {
		try {
			Parser.parse("C:/Users/Sven/Desktop/zuz2/Zug um Zug/src/Implementierung/Map");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ServerModel m;
	public Spielfeld f;
	
	@Before
	public void init() throws IOException {
		m= Parser.parse("/Zug um Zug/src/Tests/MapTest");
		f=m.getSpielfeld();
		
	}

	@Test
	public void ParserTestR�ckgabe() throws IOException {
		assertTrue("Parser R�ckgabe ist null", m!=null);
	}
	@Test
	public void ParserTestR�ckgabeModel() throws IOException {
		assertTrue("Parser R�ckgabe->Model->Spielfeld ist null", m.getSpielfeld()!=null);
		assertTrue("Parser R�ckgabe->Model->CardStack ist null",m.getCards()!=null );
		assertTrue("Parser R�ckgabe->Model->Missions ist null", m.getMissions()!=null);
	}

	@Test 
	public void ParserTestR�ckgabeSpielfeld() throws IOException{
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte",f.getCity()!=null);
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Strecken", f.getTrack()!=null);
	}
	
	@Test
	public void ParserTestR�ckgabeSpielfeldSt�dteAnzahl() throws IOException {
		Spielfeld f= m.getSpielfeld();
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: Anzahl", (f.getCity()).size()==9);
	}
	
	@Test
	public void ParserTestR�ckgabeSpielfeldSt�dteErste() throws IOException {
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->Name", (f.getCity().get(0)).getName().equals("A"));
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->Kanten ist null", (f.getCity().get(0)).getList()!=null);
	}
	
	@Test
	public void ParserTestR�ckgabeSpielfeldErsteStadtErsteKante() throws IOException {
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->erste Kante: Laenge ist falsch", 
				(f.getCity().get(0)).getList().get(0).getLength()==2);
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->erste Kante: Besitzer ist schon gesetzt", 
				(f.getCity().get(0).getList().get(0).getOwner()==(-1)));
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->erste Kante: Startid ist falsch", 
				(f.getCity().get(0).getList().get(0).getStart().getId()==0));
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->erste Kante: Endid ist falsch", 
				(f.getCity().get(0).getList().get(0).getZiel().getId()==0));
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Staedte: erste Stadt->erste Kante: Trackid ist falsch", 
				(f.getCity().get(0).getList().get(0).getId()==0));
	}

	@Test
	public void ParserTestR�ckgabeSpielfeldKanten() throws IOException {
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Kanten: ist null",f.getTrack()!=null);
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Kanten: Anzahl ist falsch", f.getTrack().size()==13);
	}
	
	@Test
	public void ParserTestR�ckgabeSpielfeldKantenErste() throws IOException {
		Spielfeld f= m.getSpielfeld();
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Erste Kante: id falsch",f.getTrack().get(0).getId()==0);
		assertTrue("Parser R�ckgabe->Model->Spielfeld->Erste Kante: farbe ist falsch", f.getTrack().get(0).getColor()=="bla");//Farbe anpassen!!Strecke
	}
	
	
	@Test
	public void ParserTestR�ckgabeCardStack() throws IOException {
		assertTrue("Parser R�ckgabe->Model->Nachziekartenstapel ist null", m.getCards()!=null);
		assertTrue("Parser R�ckgabe->Model->Nachziekartenstapel->erste Karte hei�t falsch", m.getCards().get(0)==RED);//Farbe abfragen!!anpassen
		assertEquals("Parser R�ckgabe->Model->Nachziehstapel: Anzahl der Karten ist falsch", m.getCards().size(), 30);
		assertTrue("Parser R�ckgabe->Model->Nachziehkartenstapel: 8.Karte soll gr�n sein", m.getCards().get(7)==GREEN);
	}
	
	@Test
	public void ParserTestR�ckgabeMissionStack() throws IOException {
		assertTrue("Parser R�ckgabe->Model->Auftragskartenstapel ist null", m.getMissions()!=null);
	//noch zu �ndern:	assertTrue("Parser R�ckgabe->Model->Auftragskartenstapel->erste Karte hei�t falsch", m.getMissions().get(0));
		assertEquals("Parser R�ckgabe->Model->Auftragskartenstapel: Anzahl der Karten ist falsch", m.getMissions().size(), 6);

	}
	
	@Test
	public void ParserTestAllgemein() throws IOException {
		assertTrue("Parser R�ckgabe->Model->MaxTracks ist falsch", m.getMaxtracks()==7);
		assertTrue("Parser R�ckgabe: Felder gef�llt, die leer seien sollten", (m.getOpenCards()==null)&&(m.getPlayerList()==null));
	}
	
	


}
