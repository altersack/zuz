package Tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import Implementierung.*;



public class SpielerdatenSimpleTests {
	
	SpielerdatenSimple klaus;
	Stadt a;
	Stadt b;
	Strecke ab;
	Strecke ba;

	/*
	 * SpielerdatenSimple:
	 *  - addPoints: 
	 *  - addTrack: 
	 *  - addMission: 
	 *  - Getter/Setter:
	 *  	id
	 *  	points
	 *  	name
	 *  	missioncount
	 *  	cardscount
	 *  	usedWagons
	 *  	tracksOwned
	 */
	
	@Before
	public void setUp(){
		klaus = new SpielerdatenSimple(2, "klaus");
		a = new Stadt("a", 0, 1, 1);
		b = new Stadt("b", 1, 1, 1);
		ab = new Strecke(a, b, 0, 2, "red", false);
		ba = new Strecke(b, a, 1, 6, "blue", false);
	}
	
	@Test
	public void addPointsTest() {
		klaus.addPoints(3);
		assertEquals("Klaus m�sste 3 Punkte haben", klaus.getPoints(),3);
	}
	
	
	@Test
	public void addTrackTest() {
		klaus.addTrack(ab);
		assertEquals("Klaus m�sste 2 Punkte haben", klaus.getPoints(), 2);
		assertEquals("Klaus m�sste 2 Waggons benutzt haben", klaus.getUsedWaggons(), 2);
		assertEquals("Klaus d�rfte nur noch 2 Karten haben", klaus.getNumCards(), 2);
		klaus.tookCard();
		klaus.tookCard();
		klaus.tookCard();
		klaus.tookCard();
		klaus.addTrack(ba);
		assertEquals("Klaus m�sste 17 Punkte haben", klaus.getPoints(), 17);
		assertEquals("Klaus m�sste 8 Waggons benutzt haben", klaus.getUsedWaggons(), 8);
		assertEquals("Klaus d�rfte keine Karten mehr haben", klaus.getNumCards(), 0);
	}
	
	
	@Test
	public void addMissionTest() {
		klaus.tookMissions(4);
		assertEquals("Klaus m�sste 4 Missionen haben", 4, klaus.getNumMissions());
	}

}
