package Tests;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Implementierung.ServerModel;
import Implementierung.Spielerdaten;


public class SpielerdatenTests {
	ServerModel testmodel;

	/* Spielerdaten:
	 *  - addCards: Gib dem Spieler neue Handkarten. Bei der Erhöhung sollte es im Array handcards[] veraendert sein.
	 *  - addMission: Wie addCards. Hier sollte ArrayList <mission> veraendert werden.
	 *  - getHands: Gibt die Anzahl der ausgewaehlten Farbe zurück.
	 *  - removeMission(1): Eine der drei sich im Array befindenden Missionen wird zurückgegeben. Sie ist dann nicht mehr in der ArrayList
	 *  - removeMission(2): In der ArrayList sind mehr als 3 Missionen, es möchte die vierte von hinten zurückgegeben werden. (fail)
	 *  		--- Hier wird das noch nicht getestet, weil es quasi ein performMove ist ?? TEST HIER NICHT VORHANDEN !!
	 *  - Getter/Setter:
	 *  	handcards
	 *  	missions
	 *  	failures
	 */
	
	@Before
	public void setUp() {

	ServerModel testmodel = TestSetUp.serverSetUp1();
	}
	
	@Test
	public void addCardsTest(Kind colour, int counter) {
																		// ?? Wie geht man mit Kind um ??
	}
	
	@Test
	public void addMissionTest(Mission m) {
		ArrayList<Mission> missionList;									// !! Mission muss noch irgendwo implementiert werden !!
		
		int x = missionList.size();
		assert(!missionList.contains(m));
		addMission(m);
		int y = missionList.size();
		assert(missionList.contains(m) && x+1 == y);					
		
	}
	
	@Test
	public void getHandsTest(Kind k) {
																		// ?? Wie geht man mit Kind um ??
	}
	
	@Test
	public void removeMissionTest1(Mission m) {
		ArrayList<Mission> missionList;									// ?? Evtl. @Before? Muss mindestens 3 Elemente beinhalten! ??
		assert(missionList.size() >0);
		
		removeMission(m);
		assert(!missionList.contains(m));
																		// !! Assert: andere sind noch drin, aber wo? !!
	}
	
	@Test
	public void removeMissionTest2(Mission m) {
		ArrayList<Mission> missionList;									// ?? Evtl. @Before? Muss mindestens 3 Elemente beinhalten! ??
		
		
	}
	
	@Test
	public void getHandcardsTest(Spielerdaten spielerdaten) {
		int x[] = spielerdaten.handcards;								// !! Braucht konkretes Beispiel, @Before !!
		int y[] = spielerdaten.getHandCards();
		
		int laenge = x.length;
		int laenge2 = y.length;
		assert(laenge == laenge2);
		for(int i=0;i<laenge;i++)
			assert(x[i] ==y[i]);
		
	}
	
	@Test
	public void setHandcardsTest(Spielerdaten spielerdaten) {
																		// ...
	}
	
	@Test
	public void getMissionsTest(Spielerdaten spielerdaten) {
																		// ...
	}
	
	@Test
	public void setMissionsTest(Spielerdaten spielerdaten) {
																		// ...
	}
	
	@Test
	public void getfailuresTest(Spielerdaten spielerdaten) {
																		// ...
	}
	
	@Test
	public void setfailuresTest(Spielerdaten spielerdaten) {
																		// ...
	}

}
