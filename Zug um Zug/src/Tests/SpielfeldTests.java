package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Implementierung.*;


public class SpielfeldTests {
	ServerModel testmodel;

	/* 
	 * Spielfeld:
	 *  - setzeBesitzer(1): Weise einem Spieler eine noch nicht bebaute Strecke zu
	 *  - setzeBesitzer(2): Versuche einem Spieler eine Strecke zuzuweisen, die schon bebaut wurde (failiure)
	 *  - addCity: Füge eine neue Stadt zum Spielfeld hinzu. Danach sollte es in der ArrayList cities sein.
	 *  - addTrack: Füge eine neue Strecke zum Spielfeld hinzu (neues Strecke-Objekt). Danach sollte sie in der ArrayList tracks sein.
	 */
	
	
	

	@Before
	public void setUp() {
	ClientModel testmodel = TestSetUp.clientSetUp1();
	}
	
	@Test
	public void setzeBesitzerTest1() {
		
		SpielerdatenSimple spieler;
		int idstrecke;
	
		int idspieler = spieler.getId();
		
		Spielfeld spielfeld = testmodel.getSpielfeld();
		
		idstrecke = 1;
		spielfeld.setzeBesitzer(spieler, idstrecke);
		
		
		Strecke strecke = testmodel.getSpielfeld().getTrack().get(1);
		
		assert(strecke.getOwner() == idspieler);
	}
	
	@Test
	public void setzeBesitzerTest2() {
					
		SpielerdatenSimple spieler;
		int idstrecke;
		int idspieler = spieler.getId();
		Spielfeld spielfeld = testmodel.getSpielfeld();
		idstrecke = 1;
		
		Strecke strecke = testmodel.getSpielfeld().getTrack().get(1);
		strecke.setOwner(spieler);
		
		
	}
	
	
	@Test
	public void addCityTest() {
	
		Stadt stadt = new Stadt("Borussia Dortmund", 3, 3, 3);
		
		Spielfeld spielfeld =testmodel.getSpielfeld();
		
		spielfeld.addCity(stadt);
		assert(spielfeld.getCity().get(3) != null);					// ?? Kann man so vergleichen ?? Wahrsch. OutOfBounce !!!
		
		
		
		assert(!spielfeld.getCity().contains(stadt));
		spielfeld.addCity(stadt);
		assert(spielfeld.getCity().contains(stadt));
	}
	
	@Test													
	public void addTrack() {
	
		
		Strecke strecke = new Strecke(testmodel.getSpielfeld().getCity().get(2), testmodel.getSpielfeld().getCity().get(1), 2, 2,"ALL" ,false);
		Spielfeld spielfeld = testmodel.getSpielfeld();
		
		assert(!spielfeld.getTrack().contains(strecke));
		spielfeld.addTrack(strecke);
		assert(spielfeld.getTrack().contains(strecke));
	}
	

}
