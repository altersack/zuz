package Tests;

import static org.junit.Assert.*;
import Implementierung.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Implementierung.ClientModel;
import Implementierung.Stadt;
import Implementierung.Strecke;

public class StadtTests {
	
	ServerModel testmodel;


	/* 
	 * Stadt: Was muss getestet werden?
	 *  - Kante hinzufügen. Danach ist Kante in Arraylist der Stadt
	 */
	
	@Before
	public void setUp() {
	
	ServerModel testmodel = TestSetUp.serverSetUp1();
	}
	
	
	@Test
	public void kanteHinzufuegenTest() {	
	// fügt eine noch nicht in der Streckenliste enthaltene Strecke in die Liste ein

		Spielfeld spielfeld = testmodel.getSpielfeld();
		ArrayList <Stadt> stadtlist = spielfeld.getCity();
		Stadt stadt1 = stadtlist.get(2);
		Stadt stadt2 = stadtlist.get(1);
		
		Strecke s= new Strecke(stadt1, stadt2, 2, 2,"ALL" ,false);
		// Durch das Erstellen der Strecke sollten die Staedte schon wissen, dass diese Strecken auf sie zeigen (wird schon im
		// Konstruktor gemacht!

		assert(stadt1.getList().contains(s));
		assert(stadt2.getList().contains(s));
	}
	
	
	

}
