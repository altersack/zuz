package Tests;
import java.util.ArrayList;
import Implementierung.*;


public class TestSetUp {

	public static ServerModel serverSetUp1(){
		Spielfeld map = new Spielfeld();
		//TODO add stacks
		ServerModel model = new ServerModel(map, 4, new ArrayList<String>(), new ArrayList<String>());
		Spielerdaten hans = new Spielerdaten(0,  "Hans");
		Spielerdaten karl = new Spielerdaten(1, "karl");
		Stadt berlin = new Stadt("Berlin", 0, 0, 0);
		Stadt frankfurt = new Stadt("Frankfurt", 1, 1, 1);
		Stadt mainz = new Stadt("Mainz", 2, 2, 2);
		model.addCity(berlin);
		model.addCity(frankfurt);
		model.addCity(mainz);
		model.addTrack(new Strecke(berlin, frankfurt, 0, 2,"ALL", false));
		model.addTrack(new Strecke(berlin, mainz, 1, 4,"ALL" ,false));
		model.addPlayer(hans);
		model.addPlayer(karl);
		return model;
	}
	
	public static ClientModel clientSetUp1(){
		Spielfeld map = new Spielfeld();
		//TODO add cards
		ClientModel model = new ClientModel(map, 4);
		Spielerdaten ich = new Spielerdaten(0,  "Hans");
		SpielerdatenSimple horst = new Spielerdaten(1, "karl");
		Stadt berlin = new Stadt("Berlin", 0, 0, 0);
		Stadt frankfurt = new Stadt("Frankfurt", 1, 1, 1);
		Stadt mainz = new Stadt("Mainz", 2, 2, 2);
		model.addCity(berlin);
		model.addCity(frankfurt);
		model.addCity(mainz);
		model.addTrack(new Strecke(berlin, frankfurt, 0, 2,"ALL", false));
		model.addTrack(new Strecke(berlin, mainz, 1, 4,"ALL" ,false));
		model.addPlayer(ich);
		model.addPlayer(horst);
		horst.tookCard();
		horst.tookMission(2);
		return model;
	}
	
}
